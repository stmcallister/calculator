var num1 = 0;
var num2 = 0;
var final = 0;
var operationType = '';
var firstNum = true;
var largeOp = false;

function init() {
    num1 = 0;
    num2 = 0;
    final = 0;
    operationType = '';
    firstNum = true;
    largeOp = false;
    document.getElementById("calc_screen").value = num1;
}

function displayNum(num) {
    // sets calc_screen value
    if(firstNum == true) {
        num1 += num + "";
    } else if(largeOp == true) {
        if (operationType == '+') {
            final = parseFloat(num1) + parseFloat(num2);
        } else if (operationType == '-') {
            final = parseFloat(num1) - parseFloat(num2);
        } else if (operationType == '*') {
            final = parseFloat(num1) * parseFloat(num2);
        } else if (operationType == '/') {
            final = parseFloat(num1) / parseFloat(num2);
        }
        num1 = final;
    } else {
        num2 += num + "";
    }
    if (document.getElementById("calc_screen").value == 0) {
        document.getElementById("calc_screen").value = num;
    } else {
        document.getElementById("calc_screen").value += num;
    }
    console.log(`On first number? ${firstNum}`);
    console.log(`Number 1: ${num1}, number 2: ${num2}`);
    console.log(`Final (so far): ${final}`);
}

function operate(opType) {
    if (firstNum === false) {
        largeOp = true;
    }
    if (largeOp === true) {
        firstNum += largeOp;
    }
    firstNum = false;
    operationType = opType;
    document.getElementById("calc_screen").value = 0;
}

function calculate() {

    if (operationType == '+') {
        final = parseFloat(num1) + parseFloat(num2);
        // Need to add parseFloat() to the rest
    } else if (operationType == '-') {
        final = parseFloat(num1) - parseFloat(num2);
    } else if (operationType == '*') {
        final = parseFloat(num1) * parseFloat(num2);
    } else if (operationType == '/') {
        final = parseFloat(num1) / parseFloat(num2);
    }

    console.log(`Final: ${final}`);
    document.getElementById("calc_screen").value = final;
    num1 = final;
    num2 = 0;
}

function clearCalc() {
    console.log('CLEARING');
    init();
}

document.addEventListener('keypress', event => {
    var e = event.keyCode;
    if(e == '48') {
        displayNum(0);
    } else if(e == '49') {
        displayNum(1);
    } else if(e == '50') {
        displayNum(2);
    } else if(e == '51') {
        displayNum(3);
    } else if(e == '52') {
        displayNum(4);
    } else if(e == '53') {
        displayNum(5);
    } else if(e == '54') {
        displayNum(6);
    } else if(e == '55') {
        displayNum(7);
    } else if(e == '56') {
        displayNum(8);
    } else if(e == '57') {
        displayNum(9);
    } else if(e == '106') {
        operate('*');
    } else if(e == '107') {
        operate('+');
    } else if(e == '109') {
        operate('-');
    } else if(e == '110') {
        displayNum('.')
    } else if(e == '111') {
        operate('/');
    } else if(e == '190') {
        displayNum('.');
    } else if(e == '13') {
        calculate(operationType);
    } else if(e == '27') {
        init();
    }
})